package com.cliks.registroJornada.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cliks.registroJornada.registroModel.RegistroCliks;
import com.cliks.registroJornada.repository.RegistroRepository;

@Controller
public class RegistroClicksController {

	@Autowired
	RegistroRepository registroRepository;

	@RequestMapping(path="/registro", method=RequestMethod.POST)
	@ResponseBody

	public RegistroCliks inserirAnuncio(@RequestBody RegistroCliks registroClicks) {
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
 		registroClicks.setDataDeHoje(timestamp);
		return registroRepository.save(registroClicks);
	}

	// FIM CADASTRO ANUNCIO

}
