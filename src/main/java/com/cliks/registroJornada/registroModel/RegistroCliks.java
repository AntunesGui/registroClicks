package com.cliks.registroJornada.registroModel;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@Entity - > serve para criar a classe como tabela.
@Entity
public class RegistroCliks {
	//	 @GeneratedValue(strategy=GenerationType.IDENTITY) -> serve para criar auto incremento no id.

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int    id;
	private String url;
	private String navegador;
	private String sistemaOperacional;
	public Timestamp dataDeHoje;
	//= new Timestamp(System.currentTimeMillis());


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getNavegador() {
		return navegador;
	}
	public void setNavegador(String navegador) {
		this.navegador = navegador;
	}
	public String getSistemaOperacional() {
		return sistemaOperacional;
	}
	public void setSistemaOperacional(String sistemaOperacional) {
		this.sistemaOperacional = sistemaOperacional;
	}
	public Timestamp getDataDeHoje() {
		return dataDeHoje;
	}
	public void setDataDeHoje(Timestamp dataDeHoje) {
		this.dataDeHoje = dataDeHoje;
	}

}
