package com.cliks.registroJornada.repository;

import org.springframework.data.repository.CrudRepository;
import com.cliks.registroJornada.registroModel.RegistroCliks;

public interface RegistroRepository extends CrudRepository<RegistroCliks, Integer> {

}
